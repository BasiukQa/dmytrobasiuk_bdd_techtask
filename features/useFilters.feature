Feature: Filters test cases
    As a User
    I want to able to use multiple filters,
    reset filter
    and check filters where there are no results

    Background:
        Given I'm on the Showroom page

    Scenario: Test for selecting 2 filters and verifying the result
        When I accept site cookies
        And I open filter #'1' which is called 'Popular filters'
        And I select filter option 'Best deals'
        Then I verify filter option 'Best deals' is selected
        And I open filter #'2' which is called 'Make & Model'
        And I select filter option 'Audi'
        Then I verify filter option 'make-AUDI' is selected
        Then I verify filtered 'Audi' results

    Scenario: Test if particular filter can be selected and remove it
        When I accept site cookies
        And I open filter #'1' which is called 'Popular filters'
        And I select filter option 'Best deals'
        Then I verify filter option 'Best deals' is selected
        And I reset filters
        And I open filter #'1' which is called 'Popular filters'
        Then I verify input 'Best deals' is not selected

    Scenario: Test "Make&Model" filter behavior when there no filtered results messages is displayed
        When I accept site cookies
        And I go to specific url '/en-be/business/showroom/?leaseOption[contractDuration]=60&leaseOption[mileage]=60000' and see No filtered results message
        And I open filter #'2' which is called 'Make & Model'
        Then I verify that there is no search field
