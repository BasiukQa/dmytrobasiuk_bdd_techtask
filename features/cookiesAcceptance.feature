Feature: Negative test: Test the cookies feature
    As a User
    I'm not able to use filters 'till
    I'll accept cookies

    Background:
        Given I'm on the Showroom page

    Scenario: Test if interaction with the page is possible without cookies acceptance
        Then I verify that filter #'1' which is called 'Popular filters' is not clickable
