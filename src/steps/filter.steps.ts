import { Given, When, Then } from '@cucumber/cucumber';
import filters from '../pages/filters.page'

Given(/^I'm on the Showroom page$/,()  => {
    filters.open('/en-be/business/showroom/');
    filters.verifyTheShowroomPage()
});

When(/^I accept site cookies$/, () => {
    filters.acceptCookies()
});

When(/^I open filter #'(.+)' which is called '(.+)'$/, (nth:string, filterTitle:string) => {
    filters.openNthFilter(nth, filterTitle);
});

When(/^I verify that filter #'(.+)' which is called '(.+)' is not clickable$/, (nth:string, filterTitle:string) => {
    filters.verifyThatFilterButtonIsNotClickable(nth, filterTitle);
});

When(/^I reset filters$/, () => {
    filters.resetFilters();
});

When(/^I go to specific url '(.+)' and see No filtered results message$/, (specificUrl:string)  => {
    filters.open(specificUrl);
    filters.verifyNoFilteredResultsMessageDisplaying();
});

Then(/^I select filter option '(.+)'$/, (filterOption:string) => {
    filters.selectFilter(filterOption);
});

Then(/^I verify filter option '(.+)' is selected$/, (filterOption:string) => {
    filters.verifyFilterIsSelected(filterOption);
});

Then(/^I verify input '(.+)' is not selected$/, (input:string) => {
    filters.verifyInputIsNotSelected(input);
});

Then(/^I verify filtered '(.+)' results$/, (optionSelected:string) => {
    filters.verifySelectedFilterResults(optionSelected)
})

Then(/^I verify that there is no search field$/, () => {
    filters.checkNoSearchFieldDisplaying();
});
