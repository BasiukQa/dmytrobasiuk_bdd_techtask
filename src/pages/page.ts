/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
export default class Page {

    get acceptCookiesButton(){ return $("button[class*='accept-cookies-button']")}

    // eslint-disable-next-line class-methods-use-this,require-jsdoc
    open(path:string) {
        browser.url(path);
        browser.setWindowSize(1920, 1080);
    }

    acceptCookies(){
        this.acceptCookiesButton.click()
    }

    getNumberFromSpring(value:string) {
        const arrayOfNumbers = value.match(/\d/g);
        // @ts-ignore
        const singleNumberString = arrayOfNumbers.join("");
        console.log("Our number is: ", singleNumberString)
        return +singleNumberString;
    }

    verifyInputIsNotSelected(inputID:string){
        const isSelected = $(`input[id*='${inputID}']`).isSelected();
        expect(isSelected)
                // @ts-expect-error
            .not.toEqual(true, `"${inputID}" should not be selected`);
    }

}
