import Page from './page';

class Filters extends Page {

    get showroomTitle() {return $('//h1[text()="Our business lease cars"]')}
    get saveFilterButton () { return $("//span[text()='Save']/parent::button") }
    get resetFiltersButton () { return $("button[class*='ClearButton']") }
    get filteredResults () { return $("span[data-key*='toChooseFrom']") }
    get noResultsMessage(){return $("section[id='No filter results']")}
    get searchFieldWrapper() {return $('div[class*="SearchBarWrapper"]')}
    get filter() {return ('div[data-component="desktop-filters"] div[class*="FiltersContainer"]:first-of-type>div')}

    openNthFilter (nth: string, text: string) {
        $(this.filter).waitForDisplayed()
        const filterTitle = $(this.filter+`:nth-of-type(${nth}) button h3`).getText()
        expect(filterTitle).toContain(text);
        $(this.filter+`:nth-of-type(${nth}) button`).click();
    }

    verifyThatFilterButtonIsNotClickable(nth: string, text: string){
        const filterTitle = $(this.filter+`:nth-of-type(${nth}) button h3`).getText()
        expect(filterTitle).toContain(text);
        if($(this.filter+`:nth-of-type(${nth}) button`).isClickable()) {
            throw new Error('Filters are clickable while cookies were not accepted')
        }
    }

    resetFilters(){
        this.resetFiltersButton.click()
    }

    verifyTheShowroomPage(){
        this.showroomTitle.isDisplayed()
    }

    selectFilter(filterToSelect:string){
        $(`//span[text()='${filterToSelect}']`).click()
    }

    verifyFilterIsSelected(filterToSelect:string){
        $(`input[id*='${filterToSelect}']`).isSelected()
    }

    verifySelectedFilterResults(optionSelected:string){
        browser.pause(3000)
        const selectedOptionNumber = $(`//span[text()="${optionSelected}"]/parent::span/span[2]`).getText()
        const selectedOptionNumberConverted = this.getNumberFromSpring(selectedOptionNumber);
        this.saveFilterButton.click()
        const filteredResults = this.filteredResults.getText()
        const filteredResultsConverted = this.getNumberFromSpring(filteredResults);
        expect(selectedOptionNumberConverted).toEqual(filteredResultsConverted);
    }

    verifyNoFilteredResultsMessageDisplaying(){
        this.noResultsMessage.isDisplayed()
    }

    checkNoSearchFieldDisplaying(){
        if (this.searchFieldWrapper.isDisplayed()){
            throw new Error('BUG: search field should be not displayed')
        }
    }

}

export default new Filters();
