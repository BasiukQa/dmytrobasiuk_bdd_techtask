Cucumber WebdriverIO v.7 tests
==============================

## Requirements

- Node version 12 or higher


## Install and run tests

1. `npm install`
2. `npm run wdio`

## How to write a test

Tests are written in [Gherkin syntax](https://cucumber.io/docs/gherkin/)
that means that you write down what's supposed to happen in a real language. All test files are located in
`./src/features/*` and have the file ending `.feature`. 

## Configurations

To configure your tests, checkout the [`wdio.conf.js`](https://github.com/webdriverio/cucumber-boilerplate/blob/main/wdio.conf.js) file in your test directory. It comes with a bunch of documented options you can choose from.

## Running single feature
Sometimes it's useful to only execute a single feature file, to do so use one of the commands defined in `package.json`, ex.:
`npm run useFiltersTest`

P.S. with a scenario from `verifyFiltersWithNoResults.feature` I've covered one bug, so this test will always fail

## Running CI in Gitlab
To run CI in Gitlab you can just commit and push something to the repository.
Also, it is possible to run pipeline manually through Gitlab UI.
After each run you can download artifacts of `html` test reports

P.S. currently we come into Gitlab issue which is described here: https://gitlab.com/gitlab-org/gitlab-foss/-/issues/58882
need to time for investigation it

## How to develop and improve a framework
1. Create a class for an element with constructor which will give the opportunity to chain 
   Webdriver methods like `waitForDisplayed()`, `waitForEnabled()` `scrollTo()` and `click()`

2. Install node module (like `axios` https://www.npmjs.com/package/axios) which could run API calls which e2e tests are running in case you will need
   some preconditions or go through some same actions in test multiple times
   
3. In case of using some messenger within a team (like `Slack`), create webhooks for push tests results into there 
